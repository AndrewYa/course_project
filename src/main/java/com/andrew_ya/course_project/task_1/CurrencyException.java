package com.andrew_ya.course_project.task_1;

/**
 * Created by andrew on 19.04.16.
 */
public class CurrencyException extends RuntimeException {

    public CurrencyException() {
        super("Operation on different currencies");
    }

}
