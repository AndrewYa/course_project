package com.andrew_ya.course_project.task_1;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

/**
 * Created by andrew on 19.04.16.
 */
public class Money {


    private BigDecimal value = new BigDecimal(0.0).setScale(2, RoundingMode.HALF_EVEN);
    private Currency currency;


    public Money(BigDecimal value, Currency currency) {
        this.value = value;
        this.currency = currency;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Money add(Money m) {
        if (this.currency == m.currency) {
            return new Money(this.value.add(m.getValue()), this.currency);
        } else {
            throw new CurrencyException();
        }

    }

    public Money subtract(Money m) {
        if (this.currency == m.currency) {
            return new Money(this.value.subtract(m.getValue()), this.currency);
        } else {
            throw new CurrencyException();
        }
    }

    public Money multiply(double coeff) {
        return new Money(this.value.multiply(new BigDecimal(coeff)), this.currency);
    }

    public Money divide(double coeff) {
        return new Money(this.value.divide(new BigDecimal(coeff), RoundingMode.HALF_EVEN), this.currency);
    }

    public List<Money> allocate(int n) {

        BigDecimal part = this.value.divide(BigDecimal.valueOf(n));

        List<Money> results = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            results.add(new Money(part, this.currency));
        }


        return results;
    }
}
