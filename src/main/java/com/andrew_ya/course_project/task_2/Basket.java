package com.andrew_ya.course_project.task_2;

import java.util.List;

/**
 * Created by andrew on 19.04.16.
 */
public interface Basket {

    void addProduct(String product, int quantity);
    void removeProduct(String product);
    void updateProductQuantity(String product, int quantity);
    void clear();
    List<String> getProducts();
    int getProductQuantity(String product);

}
