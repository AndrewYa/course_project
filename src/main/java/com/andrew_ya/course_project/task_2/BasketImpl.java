package com.andrew_ya.course_project.task_2;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by andrew on 19.04.16.
 */
public class BasketImpl implements Basket {

    private Map<String, Integer> basket = new HashMap<>();


    public void addProduct(String product, int quantity) {
        this.basket.put(product, quantity);
    }

    public void removeProduct(String product) {
        this.basket.remove(product);
    }

    public void updateProductQuantity(String product, int quantity) {
        this.basket.put(product, quantity);
    }

    public void clear() {
        this.basket.clear();
    }

    public List<String> getProducts() {
        List<String> result = new ArrayList<>();
        result.addAll(this.basket.keySet());
        return result;
    }

    public int getProductQuantity(String product) {
        if (this.basket.containsKey(product)) {
            return this.basket.get(product);
        } else {
            throw new ProductNotFoundException(product);
        }
    }
}
