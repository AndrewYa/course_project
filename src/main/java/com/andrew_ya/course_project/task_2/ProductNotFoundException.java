package com.andrew_ya.course_project.task_2;

/**
 * Created by andrew on 19.04.16.
 */
public class ProductNotFoundException extends RuntimeException {

    public ProductNotFoundException(String product) {
        super("Product not found: " + product);
    }
}
