package com.andrew_ya.course_project.task_3;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by andrew on 19.04.16.
 */
public class Multithreading {

    public static final int PAGE_COUNT = 5;
    public static final int REQUEST_FROM_PAGE = 50;

    private Map<String, Integer> pageCounter = new ConcurrentHashMap<>();


    public void run() {

        for (int i = 0; i < PAGE_COUNT; i++) {
            pageCounter.put("page_url" + i, 0);
        }

        for (int j = 0; j < REQUEST_FROM_PAGE; j++) {
            Visitor visitor = new Visitor("page_url" + j % 5);
            new Thread(visitor).start();
        }


        try {
            Thread.sleep(10000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        System.out.println(pageCounter.toString());
    }

    public static void main(String[] args) throws InterruptedException {
        new Multithreading().run();
    }


    private class Visitor implements Runnable {
        private String url;

        public Visitor(String url) {
            this.url = url;
        }

        public void run() {
            int visitors = pageCounter.get(url);
            pageCounter.put(url, visitors + 1);
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }

        }
    }


}