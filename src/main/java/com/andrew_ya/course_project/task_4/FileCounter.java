package com.andrew_ya.course_project.task_4;

import java.io.File;

/**
 * Created by andrew on 22.04.16.
 */
public class FileCounter {

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        int files = countFiles(new File("/tmp"));
        System.out.println("Total file number: " + files);
        System.out.println("Finished in " + (System.currentTimeMillis() - start));
    }

    private static int countFiles(File file) {
        int cnt = 0;

        File[] nestedFiles = file.listFiles() == null ? new File[]{} : file.listFiles();

        for (File child : nestedFiles) {
            if (child.isDirectory()) {
                cnt += countFiles(child);
            } else {
                ++cnt;
            }
        }
        return cnt;
    }
}
