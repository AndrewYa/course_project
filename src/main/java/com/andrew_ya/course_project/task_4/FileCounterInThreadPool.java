package com.andrew_ya.course_project.task_4;

import java.io.File;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by andrew on 22.04.16.
 */
public class FileCounterInThreadPool {

    private static AtomicInteger counter = new AtomicInteger(0);
    private static AtomicInteger threadCount = new AtomicInteger(0);
    private static ThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(10);

    public static void main(String[] args) throws InterruptedException {
        executor.setMaximumPoolSize(100);

        long start = System.currentTimeMillis();
        countFilesInThreads(new File("/tmp"));
        while (threadCount.get() > 0) {
            Thread.sleep(100);
        }
        System.out.println("Total file number: " + counter.get());
        System.out.println("Finished in " + (System.currentTimeMillis() - start));
    }

    private static void countFilesInThreads(File file) {

        File[] nestedFiles = file.listFiles() == null ? new File[]{} : file.listFiles();
        for (final File child : nestedFiles) {
            if (child.isDirectory()) {
                threadCount.incrementAndGet();
                executor.execute(() -> {
                    countFilesInThreads(child);
                    threadCount.decrementAndGet();
                });
            } else {
                counter.incrementAndGet();
            }
        }
    }
}
