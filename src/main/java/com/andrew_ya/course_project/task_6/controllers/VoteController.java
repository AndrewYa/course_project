package com.andrew_ya.course_project.task_6.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by andrew on 20.04.16.
 */
@Controller
public class VoteController {


    private enum Answer {YES, NO, SHOW}

    private static Map<Answer, Integer> answers = new ConcurrentHashMap();

    static {
        answers.put(Answer.YES, 0);
        answers.put(Answer.NO, 0);
        answers.put(Answer.SHOW, 0);
    }

    @RequestMapping(value = "/vote", method = RequestMethod.GET)
    public String welcome(Model model) {
        return "vote";
    }


    @RequestMapping(value = "/showres", method = RequestMethod.GET)
    public String getVoteResults(@RequestParam("like") String like, Model model) {

        if (like.equals("yes")) {
            int tmp = answers.get(Answer.YES);
            answers.put(Answer.YES, ++tmp);
        } else if (like.equals("no")) {
            int tmp = answers.get(Answer.NO);
            answers.put(Answer.NO, ++tmp);
        } else if (like.equals("show")) {
            int tmp = answers.get(Answer.SHOW);
            answers.put(Answer.SHOW, ++tmp);
        }

        model.addAttribute("y", String.valueOf(answers.get(Answer.YES)));
        model.addAttribute("n", String.valueOf(answers.get(Answer.NO)));
        model.addAttribute("s", String.valueOf(answers.get(Answer.SHOW)));
        return "results";
    }


}
