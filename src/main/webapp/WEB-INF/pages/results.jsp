<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head><title>Simple jsp page</title></head>
  <body>
  <h3>Нравится ли Вам эта страница - результаты голосования </h3>
  <table>
    <tr>
        <td>Да: <%= request.getAttribute("y") %></td>
        <td>Нет: <%= request.getAttribute("n") %></td>
        <td>Посмотреть результаты: <%= request.getAttribute("s") %></td>
    </tr>
  </table>
  </body>
</html>