<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head><title>Simple jsp page</title></head>
  <body>

      <c:choose>
           <c:when test="${param.x > 0 && param.x <= 50 && param.y > 0 && param.y <= 50}">
              <table>
                <c:forEach begin="1" end="${param.x}" varStatus="i">
                  <tr>
                  <c:forEach begin="1" end="${param.y}" varStatus="j">
                      <td>
                      <c:out value="${i.current * j.current}"/>
                      </td>
                  </c:forEach>
                  </tr>
                </c:forEach>
              </table>
           </c:when>
           <c:otherwise>
                  Некорректные значения параметров
           </c:otherwise>
      </c:choose>
  </body>
</html>